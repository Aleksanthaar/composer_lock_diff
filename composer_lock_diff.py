import yaml
import os
from optparse import OptionParser

project_dir = os.path.dirname(os.path.realpath(__file__))

class ComposerLockDebuger():
    def __init__(self, args, options):
        self.files    = args
        self.fromFile = options.fromfile
        self.toFile   = options.tofile
        self.dumps    = {}

    def diff(self):
        for i, file in enumerate(self.files):
            output_name = '%d_dump.yaml' % i
            current_dir = os.path.dirname(os.path.realpath(__file__))
            source_path = os.path.join(current_dir, file)

            packages        = self._load_lock(source_path)
            packages_pretty = { 'packages': { p['name']: p['version'] for p in packages } }
            self.dumps[i]   = packages_pretty['packages']
            dump_path       = os.path.join(current_dir, output_name)

            print(dump_path)

            with open(dump_path, 'w', encoding = "utf-8") as yaml_file:
                dump = yaml.dump(packages_pretty)
                yaml_file.write(dump)

        if (self.fromFile and self.toFile):
            diagnostics = []
            fromKey     = int(self.fromFile)
            toKey       = int(self.toFile)

            for package, version in self.dumps[fromKey].items():
                diagnostic = None
                targetDump = self.dumps[toKey]

                if package not in targetDump.keys():
                    diagnostic = '%s not found in target dump' % package
                elif targetDump[package] != version:
                    diagnostic = '%s found with but versions differ (%s <> %s)' % (package, version, targetDump[package])

                if diagnostic is not None:
                    diagnostics.append(diagnostic)

            print('Found %d differences.' % len(diagnostics))

            for diagnostic in diagnostics:
                print(diagnostic)

    def _load_lock(self, path):
        with open(path, 'r') as file:
            lock = yaml.load(file, Loader=yaml.BaseLoader)

        return lock['packages']

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="fromfile", help="File to compare from", default=None)
    parser.add_option("-t", "--to", dest="tofile", help="File to compare to", default=None)

    (options, args) = parser.parse_args()

    debugger = ComposerLockDebuger(args, options);

    debugger.diff()
